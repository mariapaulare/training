# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program receives a line of numbers with the welfare for each citizen
and it returns the minimal amount necessary to make every welfare equal
"""
from __future__ import print_function


def main():

    """
    Calculates the greater number in a list of numbers and then it
    counts the amount of money by making a substraction
    """

    data = open("DATA.lst", "r")

    num_citizens = int(data.readline().rstrip('\n'))
    welfare_input = data.readline().rstrip('\n')
    welfare = list(map(int, welfare_input.split()))

    greater = 0
    count_money = 0

    for i in range(0, num_citizens):

        if i == 0:
            greater = welfare[0]
        else:
            if welfare[i] > greater:
                greater = welfare[i]

    for i in range(0, num_citizens):

        if welfare[i] < greater:
            count_money = (greater - welfare[i]) + count_money

    print(count_money)

    data.close()


main()

# $ python fgomezoso.py
# 4647430
