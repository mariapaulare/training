'''
pylint fgomezoso.py

-------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 9.47/10, +0.53)

Rock, Paper, Scissors (3 players)

This program prints the first letter of the winner in an rps game.
The program prints "?" when there is a tie.
'''
from __future__ import print_function


def rps(player_1, player_2):

    ''' Calculates winner of rps'''
    if player_1["option"] == player_2["option"]:
        tie = dict()
        tie["option"] = player_1["option"]
        tie["name"] = "?"
        win = tie
    if(player_1["option"] == "rock" and player_2["option"] == "paper"):
        win = player_2
    elif(player_1["option"] == "rock" and player_2["option"] == "scissors"):
        win = player_1
    elif(player_1["option"] == "scissors" and player_2["option"] == "paper"):
        win = player_1
    elif(player_1["option"] == "scissors" and player_2["option"] == "rock"):
        win = player_2
    elif(player_1["option"] == "paper" and player_2["option"] == "rock"):
        win = player_1
    elif(player_1["option"] == "paper" and player_2["option"] == "scissors"):
        win = player_2

    return win


def main():

    ''' main function '''
    player1 = dict()
    player2 = dict()
    player3 = dict()

    data = open("DATA.lst", "r")

    player1["option"] = data.readline().rstrip('\n')
    player1["name"] = "F"
    player2["option"] = data.readline().rstrip('\n')
    player2["name"] = "M"
    player3["option"] = data.readline().rstrip('\n')
    player3["name"] = "S"

    data.close()

    if(player1["option"] != player2["option"] and
       player1["option"] != player3["option"] and
       player2["option"] != player3["option"]):
        champion = "?"
    else:
        winner = rps(player1, player2)
        winner2 = rps(winner, player3)
        champion = winner2["name"]

    print(champion)


main()

# python fgomezoso.py
# M
