/*
$java -jar checkstyle-8.23-all.jar -c google_checks-modif.xml szapata.java
Starting audit...
Audit done.
$javac szapata.java
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * Given an array of strings, return another array
 * containing all of its longest strings.
 * @author sebastian
 */
public class szapata {
  /**
   *in the platform there's just a method that i implement.
   *i put this in order to be executed,info read from the file
   *and show output
   */
  public static void main(String []args) throws IOException {
    FileInputStream in = null;
    try {
      Scanner scanner = new Scanner(new File("DATA.lst"));
      int numberOfStrings = scanner.nextInt();
      String [] inputArray = new String[numberOfStrings];
      for (int i = 0; i < numberOfStrings; i++) {
        inputArray[i] = scanner.next();
      }
      String []outputArray = (new szapata()).allLongestStrings(inputArray);
      for (int i = 0; i < outputArray.length; i++) {
        System.out.print(outputArray[i] + " ");
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * extract the longest Strings of the array.
   *
   * @return longest Strings
   */
  String[] allLongestStrings(String[] inputArray) {
    int maxSize = 0;
    int largest = 0;
    for (int i = 0;i < inputArray.length;i++) {
      if (inputArray[i].length() > maxSize) {
        maxSize = inputArray[i].length();
        largest = 1;
      } else if (inputArray[i].length() == maxSize) {
        largest++;
      }
    }
    String []outputArray = new String[largest];
    for (int i = 0,j = 0; i < inputArray.length;i++) {
      if (inputArray[i].length() == maxSize) {
        outputArray[j] = inputArray[i];
        j++;
      }
    }
    return outputArray;
  }

}

/*
$java szapata
yooooooung watermelon
*/
