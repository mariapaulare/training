#!/usr/bin/env python3
'''
$ pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function


def open_file():
    '''This function open and reads a file '''
    with open('DATA.lst', 'r') as opened_file:
        list_of_lists = []
        final_list_of_list = []
        for number in opened_file:
            list_of_lists.append(number.split())
        for inner_list in list_of_lists:
            final_list = []
            for number in inner_list:
                if '.' in number or ',' in number:
                    final_list.append(float(number))
                else:
                    final_list.append(int(number))
            final_list_of_list.append(final_list)
        return final_list_of_list


def count_win(money, target, dice):
    '''This function evaluate how much you win or lose'''
    if target in dice:
        count = (dice.count(target))
        final_value = money * count
    else:
        final_value = -money
    return final_value


for game_dice_list in open_file():
    money_dice = game_dice_list[0]
    target_dice = game_dice_list[1]
    dices = game_dice_list[2:5]
    print(count_win(money_dice, target_dice, dices))
# $ python3 alejandrohg7.py
# -10
# -2.5
# 500
# 86
