#!/usr/bin/env python3
'''
$ pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function


def open_file():
    '''This function open, reads and sort the values in a file'''
    with open('DATA.lst', 'r') as opened_file:
        list_of_lists = []
        for row in opened_file:
            row = row.rstrip("\n")
            list_of_lists.append(row)
        list_of_lists = list_of_lists[1:]
        list_of_lists.sort()
        return list_of_lists


for case in open_file():
    print(case)

# $ python3 alejandrohg7.py
# 1
# 3
# 5
# 6
# 7
