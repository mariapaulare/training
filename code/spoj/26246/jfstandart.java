import java.util.Scanner;
import java.io.IOException;

/**
 * Strange But Easy- SPOJ.
 *
 * @author jfstandart
 * @see JFstandart
 */
final class JFstandart {

    /**
     * 0.
     *
     */
    private static final int ZERO = 0;
    /**
     * 1.
     *
     */
    private static final int ONE = 1;
    /**
     * 2.
     *
     */
    private static final int TWO = 2;
    /**
     * 3.
     *
     */
    private static final int THREE = 3;
    /**
     * 5.
     *
     */
    private static final int FIVE = 5;
    /**
     * 7.
     *
     */
    private static final int SEVEN = 7;

    /**
     * Empty constructor.
     */
    private JFstandart() {

    }

    /**
     * Verify that the number is prime.
     *
     * @see cousin
     * @param number number to verify that is prime
     * @return returns true if it is prime and false otherwise
     */
    public static boolean cousin(final int number) {
        int count = TWO;
        boolean cousin = true;
        if (number % TWO == ZERO) {
            return false;
        }
        while ((cousin) && (count != number)) {
            if (number % count == ZERO) {
                cousin = false;
            }
            count++;
        }
        return cousin;
    }

    /**
     * find the list of prime numbers.
     *
     * @see main
     * @param args args
     * @throws IOException io exception
     */
    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Give me the number of repetitions");
        int repeat = sc.nextInt();
        System.out.println("Give me the first prime number");
        int first = sc.nextInt();
        System.out.println("Give me the second prime number");
        int second = sc.nextInt();
        System.out.println("Give me the third prime number");
        int third = sc.nextInt();
        int counter = ZERO;
        int auxa = first;
        int auxb = second;
        int auxc = third;
        if (auxa > auxb && auxa > auxc) {
            third = auxa;
        } else if (auxb > auxa && auxb > auxc) {
            third = auxb;
        } else {
            third = auxc;
        }

        if (auxa < auxb && auxa < auxc) {
            first = auxa;
        } else if (auxb < auxa && auxb < auxc) {
            first = auxb;
        } else {
            first = auxc;
        }
        second = (auxa + auxb + auxc) - (third + first);
        while (counter < repeat) {
            int result = (first * second) + third;
            String printer = "";
            printer = String.valueOf(result);
            System.out.println(printer + "\t");
            first = third + ONE;
            second = third + THREE;
            third = third + FIVE;
            while (!cousin(first)) {
                first = first + ONE;
            }
            while (!cousin(second)) {
                second = second + ONE;
                if (second == first) {
                    second = second + ONE;
                }
            }
            while (!cousin(third)) {
                third = third + ONE;
                if (third == second) {
                    third = third + ONE;
                }
            }
            counter = counter + 1;
        }
    }
}
