#!/usr/bin/env python3
'''
$ pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function


def open_file():
    '''This function open and reads a file '''
    with open('DATA.lst', 'r') as opened_file:
        list_of_lists = []
        for row in opened_file:
            row = row.rstrip("\n")
            list_of_lists.append(row)
        return (list_of_lists[0], list_of_lists[1:])


def geometric_addition(n_o):
    '''This function adds 6 days to the n_o till n_o igual o men_or a 31 '''
    n_o = int(n_o)
    list_payday = []
    while n_o <= 31:
        list_payday.append(n_o)
        n_o += 6
    return list_payday


def pay_day(cases):
    '''This function defines the payer '''
    friends_list = cases[0].split(' ')
    days_list = cases[1]
    who_pays = []
    days_anjan = geometric_addition(1 + friends_list.index("Anjan"))
    days_sufian = geometric_addition(1 + friends_list.index("Sufian"))
    days_alim = geometric_addition(1 + friends_list.index("Alim"))
    days_shipu = geometric_addition(1 + friends_list.index("Shipu"))
    days_sohel = geometric_addition(1 + friends_list.index("Sohel"))
    days_sumon = geometric_addition(1 + friends_list.index("Sumon"))
    for day in days_list:
        index_day = 1 + int(days_list.index(day))
        if int(day) in days_anjan:
            string_payer = "Case {}: Anjan".format(index_day)
            who_pays.append(str(string_payer))
        elif int(day) in days_sufian:
            string_payer = "Case {}: Sufian".format(index_day)
            who_pays.append(str(string_payer))
        elif int(day) in days_alim:
            string_payer = "Case {}: Alim".format(index_day)
            who_pays.append(str(string_payer))
        elif int(day) in days_shipu:
            string_payer = "Case {}: Shipu".format(index_day)
            who_pays.append(str(string_payer))
        elif int(day) in days_sohel:
            string_payer = "Case {}: Sohel".format(index_day)
            who_pays.append(str(string_payer))
        elif int(day) in days_sumon:
            string_payer = "Case {}: Sumon".format(index_day)
            who_pays.append(str(string_payer))
    return who_pays


for case in pay_day(open_file()):
    print(case)

# $ python3 alejandrohg7.py
# Case 1: Anjan
# Case 2: Alim
# Case 3: Sohel
# Case 4: Sumon
# Case 5: Shipu
