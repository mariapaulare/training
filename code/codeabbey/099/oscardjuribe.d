// $ dscanner --syntaxCheck oscardjuribe.d #linting
// $ dscanner --styleCheck oscardjuribe.d #linting
// $ dmd oscardjuribe.d  #compile


// code to solve the problem Uphill Shooting from codeabbey

import std.stdio;
import std.math;
import std.algorithm;
import std.conv;

/// method to calculate high using the time
double calculate_high (double velocity, double angle, double time) {

    return velocity * time * sin(to_radian(angle)) - (0.5 * 9.81 * time*time);
}

/// method to calculate distance after getting distance
double to_radian (double degree) {

    return (degree * 3.141592) / 180.0;
}

/// method to calculate time after using the distance
double calculate_time (double velocity, double angle, double x) {
    return x / (velocity * cos(to_radian(angle)));
}

void main() {

    // array to store slops info
    int[40] slops;

    // var to store solution
    string solution;

    // create file
    File file = File("DATA.lst", "r");

  for(int slop =0; slop < 3; slop ++) {

        // iterate and read the 40 numbers of the slop
        for(int i = 0; i < 40; i++) {
            if (i == 39){
              file.readf("%d\n", &slops[i]);
            }
            else {
              file.readf("%d ", &slops[i]);
            }
        }


        for (int shot =0; shot < 3; shot++) {
            // var velocity
            int velocity;
            // var angle
            int angle;

            // store current high
            double high;

            // store current slop
            int current_slop;

            int time;

            // read velocity and angle
            file.readf("%d %d\n", &velocity, &angle);

            // iterate over slops to find the distance
            for (double i = 0; i < 160; i += 0.01) {
                int pos = cast(int)(i / 4);
                current_slop = slops[pos];
                // get time
                time =  calculate_time(velocity, angle, i);

                // get current high value
                high = calculate_high(velocity, angle, time);

                // calculate position
                // if high is slower than current slop then hit the floor
                if (i !=0 && high  <= current_slop * 4) {
                            solution = solution ~ to!string(floor(i)) ~ ' ';
                            i = 180;
                }
            }
        }
  }
  writeln(solution);
}
// $ ./oscardjuribe
// 96 68 120 60 88 120 97 68 76
