// pmd -d oscardjuribe.java -R rulesets/java/quickstart.xml -f text
// $
//$ javac oscardjuribe.java

/*
Code to solve the problem Variable Length
Code Unpack from codeabbey
*/

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;

class Node {
  // current prefix value
  char value;
  // array of sons
  Node[] sonsArray ;
  // check if leaf
  boolean isLeaf;

  public Node(){
    // empty constructor

    // number of sons, 2 because are binaries values
    this.sonsArray = new Node[2];
  };

  public Node(char value) {
    // class constructor

    // init value
    this.value = value;
    // number of sons, 2 because are binaries values
    this.sonsArray = new Node[2];
  }
}

class PrefixTree {
  // root leaf
  private Node root;

  public PrefixTree() {
    // class constructor

    // create root with empty value
    root = new Node();
  }

  public void insertLeaf(char value, String prefix) {
    // method to insert Leaf based on prefix

    // get the root of the tree
    Node currentNode = this.root;

    // iterator
    int i;

    // var to store son position
    int position;

    // iterate over prefix string don't take the last char
    for (i = 0; i < prefix.length() - 1 ; i++) {

      // get numeric value of the char, 0 (left) or 1 (right)
      position = Character.getNumericValue(prefix.charAt(i));

      // if son doesn't exist create new node
      if( currentNode.sonsArray[position] == null ) {
        // create new node
        currentNode.sonsArray[position] = new Node();

      }
      // update next node
      currentNode = currentNode.sonsArray[position];
    }

    // get numeric value of the char, 0 (left) or 1 (right)
    position = Character.getNumericValue(prefix.charAt(i));

    // the last char is the leaf
    Node leaf = new Node(value);
    leaf.isLeaf = true;

    // add the leaf to the tree
    currentNode.sonsArray[position] = leaf;
  }

  public String binToSolution(String prefix) {
    // method to convert from bin to solution using the prefix tree

    // get the root of the tree
    Node currentNode = this.root;

    // var to store son position
    int position;

    // current solution
    String solution = "";

    // iterate over prefix string, doesn't take the last char
    for (int i = 0; i < prefix.length(); i++) {

      // get numeric value of the char, 0 (left) or 1 (right)
      position = Character.getNumericValue(prefix.charAt(i));

      // take the son
      currentNode = currentNode.sonsArray[position];
      // if node is a leaf take the value
      if(currentNode.isLeaf) {
        // concat char to solution
        solution += currentNode.value;
        // restart search in the root
        currentNode = this.root;
      }

    }

    return solution;
  }

  public void initTree(PrefixTree tree) {
    // method to create prefix tree

    tree.insertLeaf(' ', "11");
    tree.insertLeaf('a', "011");
    tree.insertLeaf('e', "101");
    tree.insertLeaf('t', "1001");
    tree.insertLeaf('s', "0101");
    tree.insertLeaf('h', "0011");
    tree.insertLeaf('o', "10001");
    tree.insertLeaf('n', "10000");
    tree.insertLeaf('i', "01001");
    tree.insertLeaf('r', "01000");
    tree.insertLeaf('d', "00101");
    tree.insertLeaf('u', "00011");
    tree.insertLeaf('l', "001001");
    tree.insertLeaf('!', "001000");
    tree.insertLeaf('c', "000101");
    tree.insertLeaf('f', "000100");
    tree.insertLeaf('m', "000011");
    tree.insertLeaf('y', "0000001");
    tree.insertLeaf('p', "0000101");
    tree.insertLeaf('g', "0000100");
    tree.insertLeaf('w', "0000011");
    tree.insertLeaf('b', "0000010");
    tree.insertLeaf('v', "00000001");
    tree.insertLeaf('j', "000000001");
    tree.insertLeaf('k', "0000000001");
    tree.insertLeaf('x', "00000000001");
    tree.insertLeaf('q', "000000000001");
    tree.insertLeaf('z', "000000000000");
  }

  public String leadingZeroes(String binaryNumber) {
    // method to add leading zeroes to the binary number

    // get string size
    int size = binaryNumber.length();

    // string after leading zeroes
    String leadingZeroesNumber = "";

    // calculate how many zeroes
    int leadingZeroes = 5 - size;

    // add first n zerores
    for (int i = 0; i < leadingZeroes; i++) {
      leadingZeroesNumber += "0";
    }

    // concat original number
    leadingZeroesNumber += binaryNumber;
    return leadingZeroesNumber;
  }

  public void solve(String base32) {
    // method to solve the challenge

    // base32 alphabet
    String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUV";

    // var to store current char in the string
    char currentChar;

    // store the binaries after decode the base32
    String binaryString = "";

    // var to store char position in the alphabet
    int position;

    // iterate over argument string chars
    for (int i = 0; i < base32.length(); i++) {
      // get current char
      currentChar = base32.charAt(i);

      // get char position
      position = alphabet.indexOf(currentChar);

      // concat each number as a binary string
      binaryString += leadingZeroes(Integer.toBinaryString(position));
    }

    System.out.println(this.binToSolution(binaryString));
  }
}

public abstract class oscardjuribe {
  public static void main(String[] args) {
    // main class

    try {
      // object to read files
      FileReader fileReader =
        new FileReader("DATA.lst");

      // wrap FileReader in BufferedReader.
      BufferedReader bufferedReader =
        new BufferedReader(fileReader);

      // read input
      String base32String = bufferedReader.readLine();

      // close readers
      fileReader.close();
      bufferedReader.close();

      // create prefix tree object
      PrefixTree tree = new PrefixTree();
      // initialize prefix tree
      tree.initTree(tree);

      // method to solve the challenge
      tree.solve(base32String);
    }
    catch(FileNotFoundException ex) {
      System.out.println("File Not Found");
    }
    catch(IOException ex) {
      System.out.println("Error reading file");
    }
  }
}
/*
$ java oscardjuribe
the public good !he has forbidden his !governors to pass
!laws of immediate which constrains them to alter their
former !systems of !government !the history awajur !charters
abolishing our most valuable !laws and altering fundamentally
*/
