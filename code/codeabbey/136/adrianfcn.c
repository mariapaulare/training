/*
$ splint adrianfcn.c
Splint 3.1.2 --- 01 Dec 2016

Finished checking --- no warnings
$ cppcheck renew135.c
Checking adrianfcn.c ...
$ gcc adrianfnc.c -o output
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXBUFF 100000

struct prefix {
  char c;
  char bin[13];
};

struct base32base2 {
  char numbase32;
  char numbase2[6];
};

static void base32tobase2(char c,char binary[]);
static void decode();
static void unionlettersbin(char s[],char binphrase[]);
static char searchprefix(char *);

int main() {

  char s[MAXBUFF] = "";
  FILE *fp;
  fp = fopen("DATA.lst","r");
  /*@-nullpass@*/
  (void)fgets(s,MAXBUFF,fp);
  decode(s);
  (void)fclose(fp);
  return 0;
}

void decode(char phrase[]) {

  unsigned int i;
  char c = '0';
  char binphrase[MAXBUFF] = "", answer[MAXBUFF] = "";
  char a[13] = "",box[2] = "";
  unionlettersbin(phrase,binphrase);
  for(i = 0;i < (unsigned)strlen(binphrase);i++) {
    box[0] = binphrase[i];
    box[1] = '\0';
    strcat(a,box);
    c = searchprefix(a);
    if(c != '+') {
      box[0] = c;
      strcat(answer,box);
      printf("%c",c);
      memset(a,0,13);
    }
  }
}

void unionlettersbin(char phrase[],char binphrase[]) {

  unsigned int i;
  char binletter[6] = "";
  for(i = 0;i < (unsigned)strlen(phrase);i++) {
    base32tobase2(phrase[i],binletter);
    strcat(binphrase,binletter);
  }

}
void base32tobase2(char c, char binary[]) {

  int i;
  struct base32base2 tablevalues[] = {{'0',"00000"},{'1',"00001"},
                                    {'2',"00010"},{'3',"00011"},
                                    {'4',"00100"},{'5',"00101"},
                                    {'6',"00110"},{'7',"00111"},
                                    {'8',"01000"},{'9',"01001"},
                                    {'A',"01010"},{'B',"01011"},
                                    {'C',"01100"},{'D',"01101"},
                                    {'E',"01110"},{'F',"01111"},
                                    {'G',"10000"},{'H',"10001"},
                                    {'I',"10010"},{'J',"10011"},
                                    {'K',"10100"},{'L',"10101"},
                                    {'M',"10110"},{'N',"10111"},
                                    {'O',"11000"},{'P',"11001"},
                                    {'Q',"11010"},{'R',"11011"},
                                    {'S',"11100"},{'T',"11101"},
                                    {'U',"11110"},{'V',"11111"}};

  for(i = 0;i < 32;i++) {
    if(c == tablevalues[i].numbase32) {
      strcpy(binary,tablevalues[i].numbase2);
      break;
    }
  }
}

char searchprefix(char psearch[]) {

  int i;
  struct prefix prefixs[] = { {' ',"11"},{'e',"101"},
                              {'t',"1001"},{'o',"10001"},
                              {'n', "10000"},{'a', "011"},
                              {'s', "0101"},{'i', "01001"},
                              {'r', "01000"},{'h', "0011"},
                              {'d', "00101"},{'l', "001001"},
                              {'!', "001000"},{'u',"00011"},
                              {'c', "000101"},{'f', "000100"},
                              {'m', "000011"},{'p', "0000101"},
                              {'g', "0000100"},{'w', "0000011"},
                              {'b', "0000010"},{'y', "0000001"},
                              {'v', "00000001"},{'j', "000000001"},
                              {'k', "0000000001"},{'x', "00000000001"},
                              {'q', "000000000001"},{'z', "000000000000"}};
  if(strlen(psearch)<2) {
    return '+';
  }
  for(i = 0; i < 28;i++) {
    if(strcmp(prefixs[i].bin,psearch) == 0) {
      return prefixs[i].c;
    }
  }
  return '+';
}

/*
$ ./output
the public good !he has forbidden his !governors to pass !laws of immediate
which constrains them to alter their former !systems of !government !the
history awajur !charters abolishing our most valuable !laws and altering
fundamentally
*/
