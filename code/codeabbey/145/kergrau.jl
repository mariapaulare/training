#=
 $julia
 julia> using Lint
 julia> lintfile("kergrau.jl")
=#
open("DATA.lst") do file
  flag = false

  for ln in eachline(file)
    if flag == false
      flag = true
      continue
    end
    i = split(ln, " ")
    bas = parse(Int64, i[1]) #base
    expo = parse(Int64, i[2]) # exponent
    residuum = parse(Int64, i[3])
    #show operation (base ^ exponent) % residuum
    println(powermod(bas, expo, residuum))
  end
end

# julia kergrau.jl
# 6415497 47633440 149944057 237692363 131543378 113801124 88101499 28612040
# 181689093 25323678 202282455 111408735 50002865 158080487 185441504
# 116231942 303675996 136537219 2306131 60703135 221267643 87243846 136815367
# 44720527 37729728 272546326 167240652 88399532 23848690
