/*
Linting with CppCheck assuming the #include files are on the same
folder as ciberstein.cpp
$ cppcheck --enable=all --inconclusive --std=c++14 ciberstein.cpp
Checking ciberstein.cpp ...

Compiling and linking using the "Dev-C++ 5.11"

$ g++.exe "C:\\...\ciberstein.cpp" -o "C:\\...\ciberstein.exe"  -I
  "C:\\...\include" -I"C:\\...\include" -I"C:\\...\include" -I"C:\\...\c++" -L
  "C:\\...\lib" -L"C:\\...\lib" -static-libgcc

Compilation results...
--------
- Errors: 0
- Warnings: 0
- Output Filename: C:\\...\ciberstein.exe
- Output Size: 1,93489265441895 MiB
- Compilation Time: 1,37s
*/
#include <bits/stdc++.h>
#include <fstream>

using namespace std;

ifstream fin("DATA.lst");

int match(string op1, string op2) {

  int c = 0;

  for(int i = 0 ; i < op1.size() ; i++) {

    if(op1[i] == op2[i])
      c++;
    }
    return c;
}

int main() {

  if(fin.fail())
    cout<<"Error DATA.lst not found";

  else {

    int no;

    fin>>no;

    vector<string> v(no);
    vector<int> vec(no);

    for(int i=0; i < no; i++)
      fin>>v[i]>>vec[i];

    for(int i = 1000 ; i <= 9999 ; i++) {
      bool B = false;
      ostringstream G;

      G << i;

      string T = G.str();

      for(int a = 0; a < v.size(); a++) {

        if(match(T , v[a]) != vec[a]) {
          B = true;
          break;
        }
      }
    if(!B)
      cout << "answer:" << endl << T << endl;
    }
  }
}
/*
$ ./ciberstein.exe
5347
*/
