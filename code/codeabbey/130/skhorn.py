#!/usr/bin/env python
"""
Problem #130 Combinations with Repetitions
"""
class CombinationsWithRepetitions:
    """
    Script to build combinations of K elements of a given set of size N.
    Some elements within the set could be duplicated.
    """
    input_array = raw_input().split(' ')

    repetitions = int(input_array[0])
    nums_set = input_array[2:]
    combinations = []
    flag = True

    i = 0
    count = 0
    pop_value = 0
    new_set = []
    done_flag = False
    while not done_flag:

        if len(new_set) < repetitions:
            if count > 0:

                while pop_value != 0:

                    if(pop_value < nums_set[i]) and (len(new_set) < repetitions):
                        if(nums_set.count(nums_set[i]) == 1) and (nums_set[i] not in new_set):
                            new_set.append(nums_set[i])

                        elif nums_set.count(nums_set[i]) > 1:
                            if(nums_set[i] in new_set and \
                                new_set.count(nums_set[i])) < nums_set.count(nums_set[i]):

                                new_set.append(nums_set[i])
                            else:
                                break

                        else:
                            break

                    else:
                        break
            else:
                new_set.append(nums_set[i])

            if len(new_set) == repetitions:
                combi = "".join(str(x) for x in new_set)
                if combi not in combinations:
                    combinations.append(combi)

        elif(len(new_set) == repetitions) and (new_set[len(new_set)-1] == nums_set[i]):
            new_set.pop()

        elif(len(new_set) == repetitions) and (new_set[len(new_set)-1] != nums_set[i]):
            new_set.pop()

        if i == len(nums_set)-1:
            only_once = True
            if only_once:
                count += 1
                only_once = False

            if count > 0:
                pop = 0
                pop_value = 0
                while pop < count:
                    if len(new_set) > 0:
                        pop_value = new_set.pop()
                        if(pop_value == nums_set[len(nums_set)-1]) and (len(new_set) == 3):
                            count = 1
                            break
                        elif(pop_value == nums_set[len(nums_set)-1]) and (len(new_set) == 2):
                            count = 2
                            break

                    if pop_value != nums_set[len(nums_set)-1]:
                        break
                    else:
                        pop += 1


        if i < len(nums_set)-1:
            if count > 0:
                if len(new_set) < repetitions:
                    i = i + 1
            else:
                i = i + 1
        else:
            i = 0
            if count == 1000:
                done_flag = True

    print " ".join(str(x) for x in combinations)


CombinationsWithRepetitions()
