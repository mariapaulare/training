/*
  $ rustfmt adrianfcn.rs
  $ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;

struct Note {
  name: String,
  value: i8,
}

impl Note {
  fn new(name: &str, value: i8) -> Note {
    Note {
      name: name.to_string(),
      value: value,
    }
  }
}

fn search_index(s: &str, notes: &Vec<Note>) -> usize {
  let mut s_copy = s.clone().to_string();
  let mut index: usize = 0;
  s_copy.pop();
  for n in notes {
    if n.name == s_copy {
      index = n.value as usize;
    }
  }
  index
}

fn frequency_notes(note: &str, notes: &Vec<Note>, index: usize) {
  let note_collect: Vec<&str> = note.split("").collect();
  let five = note_collect[(note_collect.len() - 2)]
    .trim()
    .parse::<i32>()
    .unwrap();
  let mut n = (five - 4) * 12;
  let mut aux: i8 = notes[index].value;
  while aux != notes[9].value {
    if aux > notes[9].value {
      n += 1;
      aux -= 1;
    } else {
      n -= 1;
      aux += 1;
    }
  }
  let solution = (
                  (440 as f64 *
                    (2.0_f64.powf((n as f64 / 12 as f64) as f64))
                  )
                  + 0.5
                  ) as i32;
  print!("{} ", solution);
}

fn main() -> std::io::Result<()> {
  let mut file = File::open("DATA.lst")?;
  let mut content = String::new();
  file.read_to_string(&mut content)?;
  let lines = content.lines();
  let notes = vec![
      Note::new("C", 0),
      Note::new("C#", 1),
      Note::new("D", 2),
      Note::new("D#", 3),
      Note::new("E", 4),
      Note::new("F", 5),
      Note::new("F#", 6),
      Note::new("G", 7),
      Note::new("G#", 8),
      Note::new("A", 9),
      Note::new("A#", 10),
      Note::new("B", 11),
    ];
  for l in lines {
    for c in l.split_whitespace() {
      let index: usize = search_index(&c, &notes);
      frequency_notes(&c, &notes, index);
    }
  }
  println!();
  Ok(())
}
/* $ ./adrianfcn
  831 82 392 58 37 35 740 98 139 659 69 587 33 208 466 156 196 104
  87 988 233 415
*/
