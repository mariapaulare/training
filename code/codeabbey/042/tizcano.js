/*
$ eslint tizcano.js
$
*/

function toPrint(count) {
  const blackJack = 21;
  if (count > blackJack) {
    return 'Bust';
  }
  return count;
}

function workingAces(count, inputCards) {
  const blackJack = 21;
  if (count === blackJack || inputCards.length === 0) {
    return count;
  } else if (count < blackJack) {
    return workingAces(count + 1, inputCards.slice(1));
  }
  const letterChangeVal = 9;
  return workingAces(count - letterChangeVal, inputCards.slice(1));
}

function counting(cards, inputCards) {
  const letterVal = 10;
  const currentCount = inputCards.reduce((acc, element) => {
    if (cards.includes(element)) {
      return acc + letterVal;
    }
    return acc + parseInt(element, 10);
  }, 0);
  return workingAces(
    currentCount,
    inputCards.filter((element) => element === 'A')
  );
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n');
  const inputContents = inputFile.slice(1);
  const cards = [ 'A', 'J', 'K', 'Q', 'T' ];
  const solvedObject = inputContents.map((element) =>
    toPrint(counting(cards, element.split(' ')))
  );
  const output = process.stdout.write(`${ solvedObject.join(' ') }\n`);
  return output;
}

const fileReader = require('fs');
function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node tizcano.js
output:
17 19 17 18 Bust 17 20 Bust 18 16 18 21 16 21 20 18 17 18 20 20 16 21 21 21
*/
