# $ruby-lint kergrau.rb

answer = ''
flag = false
File.open('DATA.lst', 'r') do |file|
  while line = file.gets
    unless flag
      flag = true
      next
    end
    black = 0
    deck = line.gsub(/\s+/, '').chars

    line.gsub(/\s+/, '').chars.each do |card|
      if card == 'A'
        position = deck.index(card)
        deck.insert((deck.length - 1) , deck.delete_at(position))
      end
    end

    deck.each do |card|
      case card
        when 'T', 'J', 'Q', 'K'
          black = black + 10
        when 'A'
          if black > 11
            black = black + 1
          else
            black = black + 11
          end
        else
          black = black + card.to_i
      end
    end

    if black > 21
      answer << 'Bust '
    else
      answer << "#{black} "
    end

  end
end
puts answer

# ruby kergrau.rb
# 17 19 17 18 Bust 17 20 Bust 18 16 18 21 16 21 20 18 17 18 20 20 16 21 21 21
