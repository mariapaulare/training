/*
$ rustfmt rafa1894.rs #linting
$ rustc rafa1894.rs   #compilation
*/

use std::env;
use std::fs;

fn bracketcheck(s: &str) -> char {
    let string = s;
    let mut brackets = String::new();
    for c1 in string.chars() {
        if c1 == '(' || c1 == '[' || c1 == '{' || c1 == '<' {
            brackets.push(c1);
        } else if c1 == ')' || c1 == ']' || c1 == '}' || c1 == '>' {
            let a = brackets.pop();
            if (c1 == ')' && a != Some('('))
                || (c1 == ']' && a != Some('['))
                || (c1 == '}' && a != Some('{'))
                || (c1 == '>' && a != Some('<'))
            {
                brackets.push(c1);
                break;
            }
        }
    }
    if brackets == "" {
        '1'
    } else {
        '0'
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file = &args[1];
    let inputstr = fs::read_to_string(file).expect("Error");
    let lines = inputstr.lines();
    let mut x = 0;
    for i in lines {
        if x == 0 {
            x = 1;
            continue;
        }
        print!("{}", bracketcheck(&i));
    }
}

/*
$ ./rafa1894 DATA.lst
0110010110000010001
*/
