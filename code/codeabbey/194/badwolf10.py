#!/usr/bin/env python
'''
$ pylint badwolf10.py #linting
No config file found, using default configuration
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python badwolf10.py #compilation
'''

import math

FILE = open("DATA.lst", "r").readlines()[1:]
POSTS = []
for l in FILE:
    POSTS.append([float(x) for x in l.split()])

Y = [r[1] for r in POSTS]
C_INDEX = Y.index(max(Y))
N_INDEX = []
CONVEX_P = []
CONVEX_P.append(POSTS[C_INDEX])
CONVEX = ""
PREVANGLE = 0
C_ANGLE = 0.0

for r in POSTS:
    ANGLE = []
    min_angle = 180
    for px, py in POSTS:
        if([px, py] != POSTS[C_INDEX]):
            Dx = px - POSTS[C_INDEX][0]
            Dy = py - POSTS[C_INDEX][1]
            angle = math.atan(float(Dy)/float(Dx))*180/math.pi
            if(Dx < 0 and Dy > 0):
                angle = 180 - abs(angle)
            if(Dx < 0 and Dy < 0):
                angle = 180 + abs(angle)
            Dangle = angle - PREVANGLE
            if(abs(Dangle) > 180 and Dangle < 0):
                Dangle = Dangle + 360
            if(abs(Dangle) > 180 and Dangle > 0):
                Dangle = Dangle - 360
            if min_angle > abs(Dangle):
                min_angle = abs(Dangle)
                N_INDEX = POSTS.index([px, py])
                C_ANGLE = angle

    PREVANGLE = C_ANGLE
    CONVEX += str(C_INDEX) + " "
    if POSTS[N_INDEX] in CONVEX_P:
        break
    CONVEX_P.append(POSTS[N_INDEX])
    C_INDEX = N_INDEX

print CONVEX
# pylint: disable=pointless-string-statement
'''
$ python badwolf10.py
9 14 7 4 10 15 5 16 3
'''
