#Linear Function
data=[x.split() for x in """19 -2316 329 -26496
-498 23551 -286 13375
772 73961 947 90586
-906 -53043 -198 -11979
-462 -42723 -21 -2592
384 -22786 443 -26326
72 -290 329 -2860
126 -4677 -110 2875
-781 36633 34 -2487
747 -15966 -253 5034
-305 20650 928 -60728
-270 25978 938 -91198
-83 -645 507 5845
-49 1653 417 -20715
-254 -19519 -592 -44193""".splitlines()]

def linearF(arrayOfArray):
  for array in arrayOfArray:
    a=int(array[3])-int(array[1])
    a=a/(int(array[2])-int(array[0]))
    b=int(array[3])-a*int(array[2])
    print("({} {})".format(int(a),int(b)),end=" ")

linearF(data)
