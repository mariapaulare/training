#!/usr/bin/env python3
"""
Problem #10 Linear Function
"""
class LinearFunction:
    """
    Linear function is defined by an equation:
        y(x) = ax + b
    where a and b are some constants. Determine a and b by two given points
    e.g 0 0 1 1
    output (x1, y1), (x2, y2) 
    """
    points = []
    def __init__(self):

        while True:

            line = input()
            if line:

                if len(line) < 3:
                    pass
                else:
                    coordinates = line.split()
                    self.calculate_constants(*coordinates)
            else:
                break

        text = ' '.join(self.points)
        print(text)

    def calculate_constants(self, *args):
    """
    Fn to calculate the constants needed to solve the equation
    Args:
        *args       (qarg): Data

    Returns:
        formatted point
    """
        x1 = int(args[0])
        y1 = int(args[1])
        x2 = int(args[2])
        y2 = int(args[3])

        """
        Slope(m) = (y2-y1)/(x2-x1)
        Formula: (y-y1) = m*(x-x1)
        """
        a = (y2-y1)/(x2-x1)
        b = (a*(x1))+(-1*y1)
        format_point = "({0} {1})".format(int(a), int(-1*b))
        self.points.append(format_point)

LinearFunction()
