#!/bin/bash
#
# Problem #10 Linear Function
#

# Function to calculate the following formula
# y(x) = ax + b 
#
function calculate_constants () {

    data_array=($1)
    x1="${data_array[0]}"
    y1="${data_array[1]}"
    x2="${data_array[2]}"
    y2="${data_array[3]}"

    let "y=$y2-$y1"
    let "x=$x2-$x1"
    a=$(echo "scale=1; $y/$x" | bc)
    b=$(echo "scale=1; ($a*($x1))+(-1*$y1)" | bc )
    
    
    b=${b%.*}    
    let "b=$b*-1"
    printf '(%s)' "${a%.*} ${b%.*}"

}

# Read data from text
while read -r line || [[ -n "$line" ]]; do

    len=$(echo -n "$line" | wc -c)
    count=0
    if [[ "$len" -gt 3 ]]
    then
        output_array=$(calculate_constants "$line")
        let "count+=1"
    fi

    printf '%s ' "${output_array[@]}"
done < "$1"
