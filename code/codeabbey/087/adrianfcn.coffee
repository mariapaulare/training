###
  $ coffeelint adrianfcn.coffee
    ✓ 172.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file

###
fs = require 'fs'

class Tree
  constructor: (value) ->
    @izq = new Node("-")
    @value = value
    @der = new Node("-")

class Node
  constructor: (value) ->
    @izq = "-"
    @value = value
    @der = "-"

gen_tree = (t, i) ->
  if t.value == "-"
    t.value = i
  else if i < t.value
    if t.izq == "-"
      t.izq = new Node (i)
    else
      gen_tree(t.izq, i)
  else if i > t.value
    if t.der == "-"
      t.der = new Node(i)
    else
      gen_tree(t.der, i)

print_tree = (t, str) ->
  str.push("(")
  if t.izq == "-" || t.izq == undefined
    str.push("-")
  if t.izq != "-"
    print_tree(t.izq, str)
  str.push(","+t.value + ",")
  if t.der != "-"
    print_tree(t.der, str)
  if t.der == "-" || t.der == undefined
    str.push("-")
  str.push(")")

builder_tree = (inputs) ->
  tr = new Tree("-")
  str = []
  for i in inputs
    gen_tree(tr, i)

  print_tree(tr, str)
  sol = ""
  for s in str
    sol += s
  console.log(sol)

solution = (dat) ->
  inputs = []
  for d in dat
    inputs.push(parseInt(d, 10))
  builder_tree(inputs)

main = () ->
  fs.readFile('DATA.lst', (err, dat) -> solution(dat.toString()
                                      .split("\n")[1].split(" ")))
main()
###
  $ coffee adrianfcn.coffee
  (((((((-,1,-),2,(-,3,-)),4,((-,5,-),6,(-,7,-))),8,-),9,(-,10,(-,11,-)))
  ,12,((-,13,-),14,((-,15,(-,16,((-,17,-),18,(((-,19,((-,20,-),21,-)),22,-)
  ,23,-)))),24,-))),25,-)
###
