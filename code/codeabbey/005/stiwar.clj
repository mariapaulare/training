(require '[clojure.string :as str])

(defn StringToNumber [str]
  (let [n (read-string str)]
       (if (number? n) n nil)))

(defn MinNumber [a b c]
  (if (< a b)
      (if (< a c)
          a
          c
      )
      (if (< b c);;else de if (< a b)
          b
          c
      )    
  )
)

(println "ingrese la cantidad de pruebas:")
(def s (StringToNumber (read-line)) )
(def i 0)
(def v (vector))
(while (< i s)
  (do
      (def v (conj v (test)))
      (if (= i (- s 1))
       (println v)
      )
  )
  (def i (inc i))
)

(defn test []
      (println "ingrese tres numeros separados por un espacio:")
      (def arrayData (str/split (read-line) #" ") )
      (def a (StringToNumber (get arrayData 0)) )
      (def b (StringToNumber (get arrayData 1)) )
      (def c (StringToNumber (get arrayData 2)) )
      (MinNumber a b c)
  )
