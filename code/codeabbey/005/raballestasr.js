"use strict";

/**
 * Gives the minimum of two numbers.
 * @param {number} a A number to be compared.
 * @param {number} b A number to be compared.
 * @return {number} The minimum of a and b.
*/
function minimum(a, b) {
    return (a < b) ? a : b;
}

/**
 * Gives the minimum of three numbers.
 * @param {number} a A number to be compared.
 * @param {number} b A number to be compared.
 * @param {number} c A number to be compared.
 * @return {number} The minimum of a, b and c..
*/
function minimumOfThree(a, b, c) {
    return minimum(a, minimum(b, c));
}

// IO
var fs = require("fs");
var lines = fs.readFileSync("DATA.lst").toString().split("\n");
var salida = "";
var i;
var line;
for (i = 1; i < lines.length; i += 1) {
    line = lines[i].split(" ");
    salida += minimumOfThree(Number(line[0]), Number(line[1]),
        Number(line[2])) + " ";
}
console.log(salida);
