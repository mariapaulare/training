""" Codeabbey 73: Find the distance traversed after
doing some motions on a hexagonal grid.
"""
import math

TRE = math.pi/3
DIRS = {'A':0, 'B':TRE, 'C':2*TRE, 'D':math.pi,
        'E':4*TRE, 'F':5*TRE}

def hexgrid(motions):
    """ Computes the vector sum of the given motions
    and returns its magnitude.
    """
    print(motions)
    sumx, sumy = 0, 0
    for move in motions:
        print(move)
        print(DIRS[move])
        sumx += math.cos(DIRS[move])
        sumy += math.sin(DIRS[move])
    return math.sqrt(sumx**2 + sumy**2)

def main():
    """ Read codeabbey usual test cases
    """
    data = open("DATA.lst", "r")
    cases = int(data.readline())
    aus = ""
    for _i in range(cases):
        aus += str(hexgrid(data.readline().strip())) + " "
        print(aus)
    data.close()

main()

#[rab@vaio codeabbey]$ pylint raballestasr.py 
#No config file found, using default configuration
#************* Module hexgrid
#C: 31, 0: Final newline missing (missing-final-newline)
#
#------------------------------------------------------------------
#Your code has been rated at 9.38/10 (previous run: 8.24/10, +1.14)
