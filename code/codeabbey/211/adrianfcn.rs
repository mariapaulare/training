/*
$ rustfmt --check adrianfcn.rs

$ rustc adrianfcn.rs --test -o test
$ ./test
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
$ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
  let mut file = File::open("DATA.lst")?;
  let mut contents = String::new();
  let mut characters = String::new();
  let mut my_vector = vec![];
  file.read_to_string(&mut contents)?;
  for line in contents.lines() {
    for c in line.chars() {
      if characters.matches(c).count() == 0 {
        characters.push(c);
      }
    }
    for c in characters.chars() {
      let frecuency = line.matches(c).count() as f64 / line.len() as f64;
      let entropy = frecuency.log(2.0_f64).abs();
      my_vector.push(frecuency * entropy);
    }
    let sum: f64 = my_vector.iter().sum();
    if my_vector.len() > 2 {
      print!("{} ", sum);
    }
    characters.clear();
    my_vector.clear();
  }
  Ok(())
}

/*$ ./adrianfcn
  3.8731406795131322 3.0053148568942807 3.3685225277282056 3.3921472236645345
  3.342370993177109 3.673269689515107 3.115834092163221 3.2841837197791888
  3.1924178467769684 3.4613201402110083 3.551191174465697 3.0464393446710156
  3.6368421881310122 3.0105709342684843 3.0306390622295662 3.350209029099897
  3.010570934268484 3.51602764126623

