"use strict";

/**
 * Converts a Fahrenheit temperature to Celsius.
 * @param {number} fahr The temperature in Fahrenheit..
 * @return {number} The equivalent temperature in Celsius.
*/
function fahrenheitToCelsius(fahr) {
    return Math.round(((fahr - 32) * 5) / 9);
}

var fs = require("fs");
var lines = fs.readFileSync("DATA.lst").toString().split(" ");
var salida = "";
var i;
for (i = 1; i < lines.length; i += 1) {
    salida += fahrenheitToCelsius(lines[i]) + " ";
}
console.log(salida);
