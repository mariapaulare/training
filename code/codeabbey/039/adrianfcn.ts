/*
  $ tslint adrianfcn.ts
  $ tsc adrianfcn.ts
*/
import * as fs from "fs";

function deviation(values) {
  const avgr = avegare(values);
  const sdiff = values.map((value) => {
    return (value - avgr) * (value - avgr);
  });
  return Math.sqrt(avegare(sdiff));
}

function avegare(values) {
  let sum = 0;
  for (const i of values) {
    sum += i;
  }
  return sum / values.length;
}

function solution(names, values) {
  const avgStock = avegare(values);
  const devStock = deviation(values);
  if ((avgStock * 4 * 0.01) <= devStock) {
    return names + " ";
  }
  return "";
}

function main() {
  const data = fs.readFileSync("DATA.lst", "utf8");
  const arr = data.split("\n");
  let sol = "";
  arr.pop();
  arr.shift();
  for (const i of arr) {
    const names: string[] = [];
    const values: number[] = [];
    const v = i.split(" ");
    names.push(v[0]);
    for (let j = 1; j < v.length; j++) {
      values.push(parseInt(v[j], 10));
    }
    sol += solution(names, values);
  }
  // tslint:disable-next-line:no-console
  console.log(sol);
}

main();
/*
  $ node adrianfcn.js
  DALG FANT FLNT JOOG CKCL MYTH WOWY INSX IMIX SUGR ZEOD JABA MARU
  PNSN SLVR VDKL
*/
