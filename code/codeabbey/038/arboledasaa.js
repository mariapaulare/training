/*
$ eslint jarboleda.js
$
*/
function solveFor(inputLine) {
  const variables = inputLine.split(' ');
  const alpha = parseInt(variables[0], 10);
  const beta = parseInt(variables[1], 10);
  const gamma = parseInt(variables[2], 10);
  const magicNumber = 4;
  const innerMultiplication = magicNumber * alpha * gamma;
  const innerSQRT = Math.pow(beta, 2) - innerMultiplication;
  if (innerSQRT >= 0) {
    const firstXi = (-beta + Math.sqrt(innerSQRT)) / (2 * alpha);
    const secondXi = (-beta - Math.sqrt(innerSQRT)) / (2 * alpha);
    return `${ firstXi } ${ secondXi }`;
  }
  const realPart = -beta / (2 * alpha);
  const firstXiImaginary = Math.sqrt(-innerSQRT) / (2 * alpha);
  const secondXiImaginary = -firstXiImaginary;
  return `${ realPart }+${ firstXiImaginary }i `
    .concat(`${ realPart }${ secondXiImaginary }i`);
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n').slice(1);
  const answer = inputFile.map((inputLine) => (solveFor(inputLine)));
  const output = process.stdout.write(`${ answer.join('; ') }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
8 -3; -4+8i -4-8i; -10+5i -10-5i; 1+8i 1-8i; 9 5; 9+3i 9-3i; 0 -5;
 6 4; 1+7i 1-7i; 3 -2; -8+2i -8-2i; 7 0; 4 -2; 9+5i 9-5i
*/
