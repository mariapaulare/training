/**
 * tsc ./src/wgiraldom1.tsc
 * tslint -p ./
 */
import * as fs from "fs";
function reverseString(str: string): string {
  /**
   * Reverse string str
   */
  return str
    .split("")
    .reverse()
    .join("");
}

function processChunk(fullText: string, ref: string): number[] {
  /**
   * Use fullText and ref as an excuse
   * to take the soul out of the CPU and RAM.
   * (also find ref in fullText from right to left)
   */
  const reversedFullText: string = reverseString(fullText);
  const cs: number[][] = ref.split("")
  .map((v: string, i: number) => {
    const length: number = i + 1;
    const subs: string = reverseString(ref.substr(0, length));
    const index: number = reversedFullText.indexOf(subs);
    if (index < 0) {
      return [-1, -1];
    }

    return [index + length - 1, length];
  });
  const newArr: number[][] = cs.sort((a: number[], b: number[]) => {
    if(a[1] >= b[1]) {
      return -1;
    }

    return 1;
  });

  return newArr[0];
}

function toWord(B: number, A: number): string {
  /**
   * This function converts A and B into a two-byte hexadecimal
   * representation.
   */
  const out: number = A * 4096 + B;

  return out.toString(16);
}

fs.readFile("src/doyle.txt", (err: Error | null, data: Buffer) => {
  if (err !== null) {
    return err;
  }
  fs.readFile("src/DATA.lst", (errB: Error | null, dataB: Buffer) => {
    if (errB !== null) {
      return errB;
    }
    const lines: string[] = dataB
                .toString("utf-8")
                .split("\n");
    const positions: string[] = lines[1].split(" ");
    positions.map((p: string) => {
      const position: number = parseInt(p, 10);
      const fullText: string = data.toString("utf-8", position - 4096,
                          position);
      const ref: string = data.toString("utf-8", position,
                          position + 15);
      const [A, B] = processChunk(fullText, ref);
      process.stdout.write(`${ toWord(A, B) } `);
    });
  });
});
/**
 * node wgiraldom1.js
 * 371a 20c1 20ee 42e6 4c60 3246 5950 31be 2186 448c 474e 4904 4282 306b 5c32
 *  30e7 394d 5205 40e4 4bbc 260e 354a 7583 3080 6ad6 4aa1 34f4 4288 30b8 3eeb
 *  4baf 20a3 2a12 6340 500f 4512 3763 2095 421c 7d2b 484f 87be 473b 21bc
 */
