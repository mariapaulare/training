; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kamadoatfluid.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kamadoatfluid.clj
  (:gen-class)
)

; tokenize whitespace separated string into a convenient type and container
(defn s2c [wsstr]
  (into [] (map #(Double/parseDouble %) (clojure.string/split wsstr #" ")))
)

; return a descriptive text for the given BMI
(defn BMI-txt [bmi]
  (cond
    (< bmi 18.5) "under"
    (< bmi 25.0) "normal"
    (< bmi 30.0) "over"
    :else        "obese"
  )
)

; parse DATA.lst and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [c  (s2c line)
              s  (count c)
              w  (get c 0)
              h  (get c 1)]
          (if (= s 2)
            (print (str (BMI-txt (/ w (* h h))) " "))
          )
        )
      )
    )
    (println)
  )
)

; fire it all
(defn -main [& args]
  (process_file "DATA.lst")
)

; $lein run
;  over normal under obese obese obese obese over under normal normal under
;  normal normal obese normal obese under normal obese obese normal obese under
;  obese under under obese obese obese under obese normal normal under
