(ns rounding.core
  (:gen-class))

(defn -main []
  (def num1 [7805 15333 18701 13017 2386693 8040370 5818074 -5996643 4626323 6111 8444295 6655572 3870717])
  (def den1 [1872 518 1028 1146 150 594 814 3072402 146 1720 63 770 689])
  (def answer num1)
  (defn aproximar [nu de j]
    (def entero (quot nu de))
    (def decimal (- (float(/ nu de)) entero))
    (if (>= decimal 0.5) 
      (def entero (inc entero)))
    (if (<= decimal -0.5)
      (def entero (- entero 1)))
    (def answer (assoc answer j entero)))
  (dotimes [i (count num1)]
    (aproximar (num1 i) (den1 i) i))            
  (println "La respuesta es: " answer))
