%checkcode('vanemcb.m')

fileID = fopen('DATA.lst');
cellInput = textscan(fileID,'%s');
cellWords = cell(length(cellInput{1,1}),1);
newCellWords = cell(length(cellInput{1,1}),1);
cont = 0;
y = 0;

for i=1:length(cellInput{1,1})
  cellWords{i,1}=cellInput{1,1}{i,1};
end

%Cycles to find the repeat words
for k=1:length(cellInput{1,1})
  cellFind=strfind(cellWords, cellWords{k,1});
  for x=1:length(cellFind)
    if cellFind{x,1}== 1
      cont = cont + 1;
      if cont == 2
        y=y+1;
        newCellWords{x,1}= cellWords{x,1};
      end
    end
  end
  cont = 0;
end

%Delete the empty cells
emptyCells = cellfun('isempty', newCellWords);
newCellWords(emptyCells) = [];

newCellWords=sort(newCellWords)';
disp(newCellWords);

%vanemcb
%'bec' 'bes' 'bip' 'bop' 'boq' 'box' 'byx' 'daq' 'deh' 'dih' 'dik'
%'diq' 'dix' 'dyh' 'dyk' 'gaf' 'gat' 'gex' 'gih' 'giq' 'gix' 'gok'
%'gut' 'gyf' 'gyh' 'juk' 'jus' 'jut' 'jyh' 'jyq' 'jys' 'lax' 'lif'
%'lih' 'lok' 'lot' 'luh' 'mah' 'meq' 'mes' 'mif' 'muq' 'mut' 'myq'
%'nit' 'nuf' 'rac' 'raq' 'roc' 'rop' 'ruc' 'ruf' 'vyh' 'zep' 'zes'
%'zyk' 'zyx'
