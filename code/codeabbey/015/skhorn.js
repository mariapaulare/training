/*
 * Problem #15 Maximum of array
 */
var fs = require("fs");
fs.readFileSync(process.argv[2]).toString().split("\n").forEach(function (line){

    if(line.length > 3)
    {
        var data = line.split(" ");
        maxd(data);
    }
});

function maxd(data) {

    var maxi = parseInt(data[0]);
    var mini = parseInt(data[0]);

    data.forEach(function(value){
        var item = parseInt(value);
        if(maxi > item)
        {
            maxi = item;    
        }
        else if(mini < item)
        {
            mini = item;
        }
    });

    console.log(mini+" "+maxi);
}
