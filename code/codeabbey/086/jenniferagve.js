/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

/* eslint-disable fp/no-unused-expression*/
function operation(simbol, inputInfo) {
  const eachInput = inputInfo[parseInt('1', 10)];
  const indexInput = inputInfo[parseInt('0', 10)];
  const oriDataIndex = inputInfo[parseInt('2', 10)];
  const numbersIn = inputInfo[parseInt('3', 10)];
  if (simbol === '>') {
    const indexInputN = indexInput + 1;
    if (indexInputN > [ eachInput.length - 1 ]) {
      const eachInputN = eachInput.concat([ 0 ]);
      return [ oriDataIndex, indexInputN, eachInputN ];
    }
    return [ oriDataIndex, indexInputN, eachInput ];
  } else if (simbol === '<') {
    const indexInputN = indexInput - 1;
    return [ oriDataIndex, indexInputN, eachInput ];
  } else if (simbol === '+') {
    const firstPart = eachInput.slice(0, indexInput);
    const newValue = eachInput[indexInput] + 1;
    const secondPart = eachInput.slice(indexInput + 1, indexInput.length);
    const eachInputN = firstPart.concat(newValue, secondPart);
    return [ oriDataIndex, indexInput, eachInputN ];
  } else if (simbol === '-') {
    const firstPart = eachInput.slice(0, indexInput);
    const newValue = eachInput[indexInput] - 1;
    const secondPart = eachInput.slice(indexInput + 1, indexInput.length);
    const eachInputN = firstPart.concat(newValue, secondPart);
    return [ oriDataIndex, indexInput, eachInputN ];
  } else if (simbol === '.') {
    const revCode = String.fromCharCode(eachInput[indexInput]);
    process.stdout.write(`${ revCode } `);
    return [ oriDataIndex, indexInput, eachInput ];
  } else if (simbol === ':') {
    process.stdout.write(`${ eachInput[indexInput] } `);
    return [ oriDataIndex, indexInput, eachInput ];
  } else if (simbol === ';') {
    const inputStore = (numbersIn.split(' ')).map((element) =>
      parseInt(element, 10));
    const firstPart = eachInput.slice(0, indexInput);
    const newValue = inputStore[oriDataIndex];
    const secondPart = eachInput.slice(indexInput + 1, eachInput.length);
    const oriDataIndexN = oriDataIndex + 1;
    const eachInputN = firstPart.concat(newValue, secondPart);
    return [ oriDataIndexN, indexInput, eachInputN ];
  }
  return [ oriDataIndex, indexInput, eachInput ];
}

function newPositionFix(eachCommand, indexInput, eachInput,
  instrucSize, indexCommand) {
  if (eachCommand[indexCommand] === '[') {
    return indexCommand + 1;
  } else if (eachCommand[indexCommand] === ']') {
    return indexCommand + 1;
  }
  return indexCommand;
}

function recall(eachCommand, inputInfo, instrucSize, indexCommand, flagLp) {
  const eachInput = inputInfo[parseInt('1', 10)];
  const indexInput = inputInfo[parseInt('0', 10)];
  const numbersIn = inputInfo[parseInt('3', 10)];
  if (instrucSize === 0) {
    return [ inputInfo ];
  }
  if (eachCommand[indexCommand] === '[') {
    if (eachInput[indexInput] === 0) {
      const indexCommandN = eachCommand.indexOf(']');
      const eachCommandN = eachCommand.slice(indexCommandN + 1,
        eachCommand.length);
      return recall(eachCommandN, inputInfo, eachCommandN.length, 0, false);
    }
    const eachCommandN = eachCommand.slice(indexCommand, eachCommand.length);
    const compro = newPositionFix(eachCommand, indexInput, eachInput,
      instrucSize, indexCommand);

    const opera = operation(eachCommand[compro], inputInfo);
    const inputInfoN = [ opera[1], opera[2], opera[0], numbersIn ];
    return recall(eachCommandN, inputInfoN, eachCommandN.length,
      compro + 1, true);
  } else if ((eachCommand[indexCommand] === ']') && (flagLp === true)) {
    const indexCommandN = eachCommand.indexOf('[');
    return recall(eachCommand, inputInfo, eachCommand.length,
      indexCommandN, true);
  } else if ((eachCommand[indexCommand] !== ']') && (flagLp === true)) {
    const compro = newPositionFix(eachCommand, indexInput, eachInput,
      instrucSize, indexCommand);
    const opera = operation(eachCommand[compro], inputInfo);
    const inputInfoN = [ opera[1], opera[2], opera[0], numbersIn ];
    return recall(eachCommand, inputInfoN, eachCommand.length,
      compro + 1, true);
  }
  const eachCommandN = eachCommand.slice(indexCommand + 1, eachCommand.length);
  const compro = newPositionFix(eachCommand, indexInput, eachInput,
    instrucSize, indexCommand);
  const opera = operation(eachCommand[compro], inputInfo);
  const inputInfoN = [ opera[1], opera[2], opera[0], numbersIn ];
  return recall(eachCommandN, inputInfoN, eachCommandN.length, compro, false);
}

function Interpreter(erro, contents) {
  if (erro) {
    return erro;
  }
  const inputFile = contents.split('\n');
  const commandData = String(inputFile.slice(0, 1));
  const numbersIn = String(inputFile.slice(1));

  const eachCommand = commandData.split('');
  const inputInfo = [ 0, [], 0, numbersIn ];
  const indexCommand = 0;
  const instrucSize = eachCommand.length;
  const val = recall(eachCommand, inputInfo, instrucSize, indexCommand, false);
  return val;
}

const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read all the data from the cards*/
  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    Interpreter(erro, contents));
}

fileLoad();

/**
node jenniferagve.js
input:;>;<[->+<]:>:
3 5
-------------------------------------------------------------------------
output:0 8
*/
