"use strict";
/**
$ node raballestasr.js
0 362850 9410 1 107 20 4 44 15253 508 30755 40 25 23 0
*/

/**
 * Generates a random number using linear congruence
 * @param {number} mult Multiplier
 * @param {number} add Number to be added
 * @param {number} mod The generated numbers get reduced modulo mod
 * @param {number} end Number of iterations to do
 * @return {number} The generated random number
*/
function linearCongruentialGenerator(mult, add, mod, seed, end) {
    var curr = seed;
    var i;
    for (i = 0; i < end; i += 1) {
        curr = (mult * curr + add) % mod;
    }
    return curr;
}
// IO
var fs = require("fs");
var lines = fs.readFileSync("DATA.lst").toString().split("\n");
var cases = lines[0];
var salida = "";
var i;
var line;
for (i = 1; i < lines.length; i += 1) {
    line = lines[i].split(" ");
    salida += linearCongruentialGenerator(Number(line[0]), Number(line[1]),
        Number(line[2]), Number(line[3]), Number(line[4])) + " ";
}
console.log(salida);

/**
Online JSLint output
Warnings
17.4
Unexpected 'for'.
    for (i = 0; i < end; i += 1) {
23.9
Undeclared 'require'.
var fs = require("fs");
30.0
Unexpected 'for'.
for (i = 1; i < lines.length; i += 1) {
32.42
Expected 'Number' at column 8 on the next line.
    salida += linearCongruentialGenerator(Number(line[0]), Number(line[1]),
38.0
Undeclared 'console'.
console.log(salida);
*/
