/*
$ eslint tizcano.js
$
*/

function changer(left, right, ips, element, middle) {
  if (parseInt(ips[middle][0], 36) < parseInt(element, 36)) {
    return [ middle + 1, right ];
  }
  return [ left, middle - 1 ];
}
function binarySearch(input, ips, left, right) {
  const middle = parseInt((left + right) / 2, 10);
  const pos = ips[middle];
  const posTop = ips[middle + 1];
  if (
    parseInt(pos[0], 36) <= parseInt(input, 36) &&
    parseInt(input, 36) < parseInt(posTop[0], 36)
  ) {
    if (parseInt(pos[0], 36) + parseInt(pos[1], 36) >= parseInt(input, 36)) {
      return pos[2];
    }
  }
  const changed = changer(left, right, ips, input, middle);
  return binarySearch(input, ips, changed[0], changed[1]);
}
function ipInfo(firstMistake, fisrtContents) {
  if (firstMistake) {
    return firstMistake;
  }
  return fisrtContents.split('\n').map((element) => element.split(' '));
}
function solver(mistake, contents, ips) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n');
  const left = 0;
  const right = ips.length - 1;
  const solvedArray = inputFile
    .slice(1)
    .map((element) => binarySearch(element, ips, left, right));
  const output = process.stdout.write(`${ solvedArray.join(' ') }\n`);
  return output;
}

const fileReader = require('fs');
function fileLoad() {
  return fileReader.readFile(
    'db-ip.txt',
    'utf8',
    (firstMistake, fisrtContents) => {
      const ipsFile = ipInfo(firstMistake, fisrtContents);
      return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
        solver(mistake, contents, ipsFile)
      );
    }
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node tizcano.js
output:
GB PK FR AU ZA US AU CN BH AL PL UG US HK PH IN US TW US BI PT AZ CA NL IN FR
ES ES JP RS US CO DE PL AU IN GR MX AE AF JP AR PR US EE IT US HN BD PL UA US
...
IT US US PA LB NL LB GB DE US SC IT AU CH DE FR RU HR US BR US RU JP DE US FR
*/
