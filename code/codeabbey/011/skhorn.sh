#!/bin/bash

#
# Problem #11 Sum of digits
# 
function calculate () {

    array=($1)
    let "calc=${array[0]}*${array[1]}+${array[2]}"
    echo "$calc"
}

function number_to_digits () {
    
    division=$1
    count=0
    while [ "$division" -gt 0 ]
    do
        remainder[$count]=$((division%10))
        division=$(echo "$division/10" | bc)
        let "count+=1"
    done

    count=0
    for (( idx=${#remainder[@]}-1 ; idx>=0 ; idx-- )) ; do
        remainder_reversed[$count]="${remainder[idx]}"
        let "count+=1"
    done

    echo "${remainder_reversed[@]}"
}

function sum_of_digits () {

    sum_of=0
    for item in $1;
    do
        let "sum_of+=$item"
    done

    echo "$sum_of"
}

while read -r line || [[ -n "$line" ]]; do
    len=$(echo -n "$line" | wc -c)
    count=0
    if [[ "$len" -gt 3 ]]
    then
        result=$(calculate "$line")
        result=$(number_to_digits "$result")
        output_array=$(sum_of_digits "$result")
        let "count+=1"
    fi

    printf '%s ' "${output_array[@]}"
done < "$1"
