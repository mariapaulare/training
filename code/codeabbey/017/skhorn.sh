#!/bin/bash
#
# Problem #17 Array Checksum
#
while read -r line || [[ -n "$line" ]];
do
    array=($line)
    len="${#array[@]}"
    LIMIT=10000007
    if [[ "$len" -gt 3 ]];
    then
        result=0
        for item in "${array[@]}";
        do
            value=$item
            
            result=$(echo "$result"+"$value" | bc )
            result=$(echo "$result"*113 | bc)

            if [[ "$result" -gt $LIMIT ]]
            then
                result=$(echo "$result"%"$LIMIT" | bc)
            fi 
        done 

        printf '%s ' "$result"
    fi

done < "$1"
