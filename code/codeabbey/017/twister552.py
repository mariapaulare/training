data=[int(x) for x in '53276960 31116 805580 684 2018 91570638 707760379 5089000 1000 4961 3 3980 271939041 901 48151 36537483 4 6002650 90 607 1 919 24315182 15383 908 261399 13 99 5849 951329092'.split()]
def checkSum(args):
    SEED=113
    LIMIT=10000007
    result=0
    for x in args:
        result=(result+x)*SEED%LIMIT
    return result
print(checkSum(data))
