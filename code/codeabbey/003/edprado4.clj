(ns sumsinloops.core
  (:gen-class))

(defn -main []
  (def v1 [784876 539558 210573 988070 673093 52960 399793 874200 82076 805666 703103])
  (def v2 [608421 13075 686875 756295 105901 208412 483294 757918 730078 416316 417254])
  (def answer (map + v1 v2))
  (println "La respuesta es: " answer))
