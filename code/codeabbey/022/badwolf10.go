/*
$ golint badwolf10.go #linting
$ go tool compile -e badwolf10.go #compilation
*/

package main

import (
  "bufio"
  "fmt"
  "log"
  "os"
  "strconv"
  "strings"
)

func main() {

  input, err := os.Open("DATA.lst")
  if err != nil {
    log.Fatal(err)
  }

  wordScanner := bufio.NewScanner(input)

  for wordScanner.Scan() {
    inLine := strings.Split(wordScanner.Text(), " ")

    if len(inLine) > 1 {
      x, _ := strconv.Atoi(inLine[0])
      y, _ := strconv.Atoi(inLine[1])
      n, _ := strconv.Atoi(inLine[2])

      pages := 0
      nx := 1
      ny := 1
      sec := 0
      for pages < n {
        sec++
        if sec == nx*x {
          pages++
          nx++
        }
        if sec == ny*y {
          pages++
          ny++
        }

      }
      fmt.Printf("%d ", sec)
    }
  }
}

/*
$ go run badwolf10.go
318244719 13158054 278833660 132435810 51272717 46729010 8760609 107074208
309055713 33295378 322580664 359769725 332892224 162592318 175123025
*/
