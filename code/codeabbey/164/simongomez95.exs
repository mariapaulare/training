_ = """
➜  164 git:(sgomezatfluid) ✗ mix credo --strict sgomezatfluid.exs
Checking 1 source file ...

Please report incorrect results: https://github.com/rrrene/credo/issues

Analysis took 0.1 seconds (0.00s to load, 0.1s running checks)
12 mods/funs, found no issues.
"""

defmodule PageRank do
  def lines(string) do
    String.split(string, "\n", trim: true)
  end

  def indiv(line) do
    inds = String.split(line, " ", trim: true)
    {{a, ""}, {b, ""}} = {Integer.parse(hd(inds)), Integer.parse(hd(tl(inds)))}
    {a, b}
  end

  def data_initial(filename) do
    file = File.read!(filename)
    file |> PageRank.lines
      |> hd
      |> indiv
  end

  def data_body(filename) do
    file = File.read!(filename)
    file  |> PageRank.lines
      |> tl
      |> Enum.map(&indiv/1)
  end

  def build_graph do
    filename = "DATA.lst"
    {nvrt, _} = data_initial filename
    digraph = :digraph.new
    add_verts digraph, nvrt
    build_edgs digraph, data_body filename
    digraph
  end

  def add_verts(graph, nvrt) do
    for vrt <- 0..nvrt - 1, do: :digraph.add_vertex graph, vrt
  end

  def build_edgs(graph, edgs) do
    for e <- edgs, do: add_edg graph, e
  end

  def add_edg(graph, edg) do
    {v1, v2} = edg
    :digraph.add_edge graph, v1, v2
  end

  def run_graph(_, final, 0, state) do
    current_map = Agent.get(state, fn(a) -> a end)
    Agent.update state, &Map.put(&1, final, Map.get(current_map, final) + 1)
  end

  def run_graph(graph, initial, steps, state) do
    neighbours = :digraph.out_neighbours(graph, initial)
    current = Enum.random neighbours
    run_graph(graph, current, steps - 1, state)
  end

  def state_init(verts) do
    map = Map.new(verts, fn x -> {x, 0} end)
    {:ok, agent} = Agent.start_link fn -> map end
    agent
  end

end

graph = PageRank.build_graph
verts = :digraph.vertices graph
state = PageRank.state_init verts
Enum.map(verts, fn(v) -> for _n <- 1..50, do:
    PageRank.run_graph(graph, v, 20, state) end)
results = Agent.get(state, fn(a) -> a end)
rlist = Map.values(results)
"#{Enum.map(rlist, fn(c) -> Integer.to_string(c)<>" " end)}" |> IO.puts

_ = """
➜  164 git:(sgomezatfluid) ✗ elixir sgomezatfluid.exs
18 33 63 38 103 27 63 92 45 22 31 69 46
"""
