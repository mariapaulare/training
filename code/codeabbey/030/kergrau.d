/*
dub run dscanner -- -S kergrau.d
Building package dscanner in /home/grau/.dub/
packages/dscanner-0.5.11/dscanner/
Running pre-generate commands for dscanner...
Performing "debug" build using /usr/bin/dmd for x86_64.
stdx-allocator 2.77.4: target for configuration "library" is up to date.
emsi_containers 0.8.0-alpha.9: target for
configuration "unittest" is up to date.
libdparse 0.9.9: target for configuration "library" is up to date.
dsymbol 0.4.8: target for configuration "library" is up to date.
inifiled 1.3.1: target for configuration "library-quiet" is up to date.
libddoc 0.4.0: target for configuration "lib" is up to date.
dscanner 0.5.11: building configuration "application"...
Linking...
To force a rebuild of up-to-date targets, run again with --force.
Running ../../.dub/packages/dscanner-0.5.11/dscanner/bin/dscanner -S kergrau.d

*/

import std.range, std.stdio, std.algorithm;

void main()
{

  auto file = File("DATA.lst"); // Open for reading
  auto range = file.byLine();

  foreach (line; range){

    char[] s1 = removechars(line," ").dup;
      writeln(s1.reverse());
  }
}
/*
rdmd kergrau.d
sutcac tuoba ydrapoej flehs ffo reppus eraf no
*/
