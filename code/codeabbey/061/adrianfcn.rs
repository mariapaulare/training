/*
$ rustfmt adrianfcn.rs
$ rustc --test adrianfcn.rs
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
$ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;

fn sieve_of_eratosthenes(n : i64) -> Vec<i64> {
  let mut nature_numbers = vec![];
  let mut prime_list = vec![];
  for i in 2..n + 1 {
    nature_numbers.push(i);
  }
  let len = nature_numbers.len();
  for i in 0..len {
    let vl = nature_numbers[i];
    if vl.pow(2) <= n && vl != 0 {
      for j in 2..(n/vl) as i64 +1 {
        nature_numbers[((vl*j) - 2) as usize] = 0;
      }
    }
  }
  for i in nature_numbers {
    if i != 0{
      prime_list.push(i);
    }
  }
  prime_list
}

fn main() -> std::io::Result<()> {
  let mut file = File::open("DATA.lst")?;
  let mut contents = String::new();
  let n = 2750131;
  file.read_to_string(&mut contents)?;
  let mut lines = contents.lines();
  lines.next();
  let prime_list = sieve_of_eratosthenes(n);
  for l in lines {
    for c in l.split_whitespace() {
      let param : usize = c.trim().parse()
      .ok()
      .expect("only numbers");
      print!("{} ", prime_list[param - 1]);
    }
  }
  Ok(())
}

/*
  ./adrianfcn
  1521991 2635883 1340153 1648253 2043817 2386247 1986889 1540073 2544229
  1940201 2464349 1724861
*/
