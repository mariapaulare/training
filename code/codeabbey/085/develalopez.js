/*
Problem 85: Rotation in 2D Space
With JavaScript - Node.js
Linting: eslint develalopez.js

--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

const CONSTANT = 180;

function toRadians(angle) {
  return angle * (Math.PI / CONSTANT);
}

function sortingFunctionSecondCol(first, second) {
  if (first[1] === second[1]) {
    return 0;
  }
  return (first[1] < second[1]) ? 0 - 1 : 1;
}

function sortingFunctionThirdCol(first, second) {
  if (first[2] === second[2]) {
    return sortingFunctionSecondCol(first, second);
  }
  return (first[2] < second[2]) ? 0 - 1 : 1;
}

function rotate(star, angle) {
  const [ name, coordX, coordY ] = star.split(' ');
  const radAngle = toRadians(angle);
  const newX = Math.round(
    (coordX * Math.cos(radAngle)) - (coordY * Math.sin(radAngle)));
  const newY = Math.round(
    (coordY * Math.cos(radAngle)) + (coordX * Math.sin(radAngle)));
  return [ name, newX, newY ];
}

function dataProcess(erro, contents) {
  if (erro) {
    return erro;
  }
  const dataLines = contents.split('\n');
  const [ firstLine ] = dataLines;
  const [ , angle ] = firstLine.split(' ');
  const stars = dataLines.splice(1);
  const rotatedStars = stars.map((star) =>
    rotate(star, angle)).sort(sortingFunctionThirdCol);
  const output = rotatedStars.map((star) =>
    process.stdout.write(`${ star[0] } `));
  return output;
}

const filesRe = require('fs');
function main() {
  return filesRe.readFile('DATA.lst', 'utf-8', (erro, contents) =>
    dataProcess(erro, contents));
}

main();

/*
$ node develalopez.js
Algol Deneb Thabit Aldebaran Mira Sirius Kastra Heka Electra Capella Gemma
Rigel Mizar Albireo Yildun Polaris Unuk Altair Media Fomalhaut Zosma Diadem
Betelgeuse Jabbah Lesath Nembus Pherkad Procyon Alcyone Bellatrix Castor Alcor
Kochab Vega
*/
