"""
$ pylint 2.3.1#linting
$ python 3.6.6 #compilation
$ python juanes9327.py
$
"""


def latest(scores):

    """
    This function shows the latest score of a player
    """
    length = len(scores)
    latest_score = scores[length-1]
    return latest_score


def personal_best(scores):
    """
    This function shows the personal best score of a player
    """
    organized_score = sorted(scores)
    length = len(scores)
    person_best = organized_score[length-1]
    return person_best


def personal_top_three(scores):
    """
    This function shows the top three scores of a player
    """
    sorted_score = sorted(scores)
    top_three = sorted_score[::-1]
    return top_three[:3]


# My solution's last line.

# $ ./juanes9327
# ..[30, 20, 10]
# .[100, 90, 70]
# .[70, 30]
# .[40, 40, 30]
# .[40]
